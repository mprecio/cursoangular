import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(fb: FormBuilder) { 
    this.onItemAdded = new EventEmitter();

    this.fg = fb.group({    
      nombre: ['', [
        Validators.required,
        this.nombreValidador,
        this.nombreValidatorParametrizable(this.minLongitud)
      ]],
      url: ['']
    });

    this.fg.valueChanges.subscribe((form: any) =>{
      console.log('Cambio el formulario:' + form);
    });
  }

  ngOnInit(): void {
  }
  
  guardar(nombre: string, url: string): boolean{
    
    let d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }
  
  nombreValidador(control: FormControl): { [s:string]:Boolean }{    
    let l = control.value.toString().trim().length;
    if(l> 0 && l<5){
      return {invalidNombre:true};
    }
    return null;
  }

  nombreValidatorParametrizable(minLong:number) : ValidatorFn{
    return (control:FormControl) : {[s:string]:boolean} | null => {
      let l = control.value.toString().trim().length;
      if(l> 0 && l<minLong){
        return {minLongNombre:true};
      }
      return null;
    }
  }

  validInput(field: string): boolean {
    return this.fg.get(field).invalid && this.fg.get(field).touched;
  }

}
