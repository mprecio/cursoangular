import {v4 as uuid} from 'uuid';

export class DestinoViaje{

    private selected:boolean = false;
    public servicios: string[];

    nombre:string;
    imageUrl:string;
    id = uuid();

    constructor(n:string, u:string){
        this.nombre = n 
        this.imageUrl = u
        this.servicios = ['Piscina', 'Desayuno'];
    }

    isSelected(){
        return this.selected;
    }

    setSelected(s:boolean){
        this.selected = s;
    }
}
