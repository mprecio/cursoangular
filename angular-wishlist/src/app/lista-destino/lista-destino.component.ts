import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinosApiClient } from '../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css'],
  providers: [DestinosApiClient]
})

export class ListaDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  
  updates: string[];
  destinos: DestinoViaje[] = [];

  constructor(private destinosApiClient: DestinosApiClient) { 
    this.onItemAdded = new EventEmitter();    
    this.updates = [];
    this.destinosApiClient.suscribeOnChange((d:DestinoViaje) => {
      if(d != null){
        this.updates.push('Se ha elegido a ' + d.nombre);
      }
    });
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string): boolean{
    this.destinos.push(new DestinoViaje(nombre,url));
    return false;
  }

  elegido(d:DestinoViaje){
    this.destinosApiClient.elegir(d);
  }

  agregado(d:DestinoViaje){
    this.destinosApiClient.add(d);
    this.destinos.push(d);
    this.onItemAdded.emit(d);
  }

}
